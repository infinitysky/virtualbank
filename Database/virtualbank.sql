/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50549
Source Host           : localhost:3306
Source Database       : virtualbank

Target Server Type    : MYSQL
Target Server Version : 50549
File Encoding         : 65001

Date: 2016-09-28 02:31:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for accountinfo
-- ----------------------------
DROP TABLE IF EXISTS `accountinfo`;
CREATE TABLE `accountinfo` (
  `accountId` int(20) NOT NULL,
  `accountBank` varchar(10) COLLATE utf8_bin NOT NULL,
  `accountNumber` varchar(255) COLLATE utf8_bin NOT NULL,
  `createDate` date DEFAULT NULL,
  `accountAmount` int(10) unsigned zerofill NOT NULL,
  PRIMARY KEY (`accountId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of accountinfo
-- ----------------------------

-- ----------------------------
-- Table structure for account_to_customer
-- ----------------------------
DROP TABLE IF EXISTS `account_to_customer`;
CREATE TABLE `account_to_customer` (
  `account_user_Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `accountId` int(20) NOT NULL,
  `userId` int(20) NOT NULL,
  PRIMARY KEY (`account_user_Id`),
  KEY `fk_customerId` (`userId`),
  KEY `fk_accountId` (`accountId`),
  CONSTRAINT `fk_accountId` FOREIGN KEY (`accountId`) REFERENCES `accountinfo` (`accountId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_customerId` FOREIGN KEY (`userId`) REFERENCES `customerinfo` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of account_to_customer
-- ----------------------------

-- ----------------------------
-- Table structure for customerinfo
-- ----------------------------
DROP TABLE IF EXISTS `customerinfo`;
CREATE TABLE `customerinfo` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `userFirstName` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `userLastName` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `userDOB` date DEFAULT NULL,
  `userJoinDate` date NOT NULL,
  `userAddress` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `userPhoneNumber` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of customerinfo
-- ----------------------------

-- ----------------------------
-- Table structure for login
-- ----------------------------
DROP TABLE IF EXISTS `login`;
CREATE TABLE `login` (
  `loginId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `userName` varchar(255) COLLATE utf8_bin NOT NULL,
  `userPassword` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`loginId`),
  KEY `loginCustomerId` (`userId`),
  CONSTRAINT `loginCustomerId` FOREIGN KEY (`userId`) REFERENCES `customerinfo` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of login
-- ----------------------------

-- ----------------------------
-- Table structure for transanctioninfo
-- ----------------------------
DROP TABLE IF EXISTS `transanctioninfo`;
CREATE TABLE `transanctioninfo` (
  `transactionId` bigint(20) NOT NULL AUTO_INCREMENT,
  `transactionTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `transactionInfo` varchar(255) COLLATE utf8_bin NOT NULL,
  `transactionStaus` int(5) DEFAULT NULL,
  `transactionAmount` int(10) unsigned zerofill NOT NULL,
  `transactionType` varchar(255) COLLATE utf8_bin NOT NULL,
  `transactionAccountId` int(11) NOT NULL,
  PRIMARY KEY (`transactionId`),
  KEY `transactionAccountId_accountId_fk` (`transactionAccountId`),
  CONSTRAINT `transactionAccountId_accountId_fk` FOREIGN KEY (`transactionAccountId`) REFERENCES `accountinfo` (`accountId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of transanctioninfo
-- ----------------------------
